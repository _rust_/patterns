trait Product {
    fn get_price(&self) -> u32;
}

enum ProductType {
    TypeA,
    TypeB,
}

struct ProductTypeA {
    price: u32,
}

impl Default for ProductTypeA {
    fn default() -> Self {
        Self { price: 30 }
    }
}

impl Product for ProductTypeA {
    fn get_price(&self) -> u32 {
        self.price
    }
}

struct ProductTypeB {
    price: u32,
}

impl Default for ProductTypeB {
    fn default() -> Self {
        Self { price: 15 }
    }
}

impl Product for ProductTypeB {
    fn get_price(&self) -> u32 {
        self.price
    }
}

trait ProductFactory {
    fn create() -> Box<dyn Product>;
}

pub struct ProductFactoryA;

impl ProductFactory for ProductFactoryA {
    fn create() -> Box<dyn Product> {
        Box::new(ProductTypeA::default())
    }
}

struct ProductFactoryB;

impl ProductFactory for ProductFactoryB {
    fn create() -> Box<dyn Product> {
        Box::new(ProductTypeB::default())
    }
}

struct ProductFactoryWithType;
impl ProductFactoryWithType {
    fn create(pruduct_type: ProductType) -> Box<dyn Product> {
        match pruduct_type {
            ProductType::TypeA => Box::new(ProductTypeA::default()),
            ProductType::TypeB => Box::new(ProductTypeB::default()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_product_from_factory() {
        let product_a = ProductFactoryA::create();
        let product_b = ProductFactoryB::create();

        assert_eq!(product_a.get_price(), 30);
        assert_eq!(product_b.get_price(), 15);
    }

    #[test]
    fn create_product_from_factory_with_type() {
        let product_a = ProductFactoryWithType::create(ProductType::TypeA);
        let product_b = ProductFactoryWithType::create(ProductType::TypeB);

        assert_eq!(product_a.get_price(), 30);
        assert_eq!(product_b.get_price(), 15);
    }
}
